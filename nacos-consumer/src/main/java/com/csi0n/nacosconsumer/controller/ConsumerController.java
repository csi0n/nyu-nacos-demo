package com.csi0n.nacosconsumer.controller;

import com.csi0n.nacosproviderapi.NacosHelloRemoteApi;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@Api(tags = "Consumer 测试")
@RestController
@RequestMapping("/api")
public class ConsumerController {
    @Autowired
    protected NacosHelloRemoteApi nacosHelloRemoteApi;

    @ApiOperation("测试接口-获取服务提供者提供的信息")
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public Map<String, Map<String, String>> index() {

        Map<String, Map<String, String>> result = new HashMap<>();
        result.put("testKeyFromProvider", nacosHelloRemoteApi.test());

        return result;
    }

}
