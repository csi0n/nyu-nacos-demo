package com.csi0n.nacosproviderapi;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

@Component
@FeignClient(contextId = "nacosHelloRemoteApi", value = "nacos-provider-biz")
public interface NacosHelloRemoteApi {
    @RequestMapping(value = "/api/test", method = RequestMethod.GET)
    Map<String, String> test();
}
